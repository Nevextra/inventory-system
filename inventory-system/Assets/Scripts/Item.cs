﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Item {
	public string itemName;
	public int itemID;
	public string itemDesc;
	public Texture2D itemIcon;
	public int itemPower;
	public int itemValue;
	public ItemType itemtype;

	public enum ItemType {
		Weapon,
		Shield,
		Helmet,
		ChestArmour,
		Gauntlets,
		Leggings,
		Rings,
		Consumable,
		Quest
	}

	public Item(string name, int id, string desc, string iconName, int power, int value, ItemType type){
		itemName = name;
		itemID = id;
		itemDesc = desc;
		itemIcon = Resources.Load<Texture2D> ("Item Icons/" + iconName);
		itemPower = power;
		itemValue = value;
		itemtype = type;
	}

	public Item(){
		itemID = -1;
	}
}
