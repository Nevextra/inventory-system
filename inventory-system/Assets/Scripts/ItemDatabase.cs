﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemDatabase : MonoBehaviour {
	public List<Item> items = new List<Item> ();

	void Start(){
		items.Add(new Item("Health Potion", 1,"Restores health", "potion red", 5, 50, Item.ItemType.Consumable));
		items.Add(new Item("Mana Potion", 2,"Restores mana", "potion blue", 5, 50, Item.ItemType.Consumable));
		items.Add(new Item("Knight Sword", 3,"A knight's closest friend", "weapon knight sword", 30, 100, Item.ItemType.Weapon));
		items.Add(new Item("King's Sword", 100,"The Royal Treasure", "weapon king sword", 5000, 1000000, Item.ItemType.Weapon));
		items.Add(new Item("High Noon", 50,"Weapon of unknown origin", "weapon pistol revolver", 5, 50, Item.ItemType.Quest));
	}
}
