﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Inventory : MonoBehaviour {
	public int slotsX, slotsY;
	public GUISkin Skin;
	public List<Item> inventory = new List<Item> ();
	public List<Item> slots = new List<Item> ();
	private bool showInventory;
	private ItemDatabase database;
	private bool showTooltip;
	private string tooltip;

	private bool draggingItem;
	private Item draggedItem;
	private int prevIndex;


	// Use this for initialization
	void Start () {
		for(int i = 0;i < slotsX * slotsY; i++){
			slots.Add(new Item());
			inventory.Add (new Item ());
		}
		database = GameObject.FindGameObjectWithTag ("ItemDatabase").GetComponent<ItemDatabase>();
		AddItem(50);
		AddItem(50);
		AddItem (100);
		AddItem (1);
		AddItem (1);
		AddItem (2);
	}

	void Update(){
		if (Input.GetButtonDown("Inventory")) {
			showInventory = !showInventory;
		}
	}
	
	void OnGUI(){
		if (GUI.Button (new Rect (40, 200, 100, 40), "Save"))
			SaveInventory ();
		if (GUI.Button (new Rect (40, 250, 100, 40), "Load"))
			LoadInventory ();
		
		tooltip = "";
		GUI.skin = Skin;
		if(showInventory){
			DrawInventory ();
			if(showTooltip)
				GUI.Box (new Rect (Event.current.mousePosition.x + 10f, Event.current.mousePosition.y + 20f, 200, 200), tooltip,Skin.GetStyle("tooltip"));
			
		}
		if(draggingItem){
			GUI.DrawTexture (new Rect(Event.current.mousePosition.x, Event.current.mousePosition.y, 50, 50),draggedItem.itemIcon);
		}
	}

	void DrawInventory(){
		Event e = Event.current;
		int i = 0;
		for (int y = 0; y < slotsY; y++){
			for(int x = 0; x < slotsX; x++){
				Rect slotRect = new Rect (x * 60, y * 60, 50, 50);
				GUI.Box (new Rect(x * 60, y * 60, 50, 50), "", Skin.GetStyle("Slot"));
				slots [i] = inventory [i];
				Item item = slots [i];

				if (slots [i].itemName != null) {
					GUI.DrawTexture (slotRect, item.itemIcon);
					if (slotRect.Contains (e.mousePosition)) {
						tooltip = CreateTooltip (item);
						showTooltip = true;

						if (e.button == 1 && e.type == EventType.mouseDrag && !draggingItem) {
							draggingItem = true;
							prevIndex = i;
							draggedItem = item;
							inventory [i] = new Item ();
						}
						if (e.type == EventType.mouseUp && draggingItem) {
							inventory [prevIndex] = inventory [i];
							inventory [i] = draggedItem;
							draggingItem = false;
							draggedItem = null;
						}
						if(e.isMouse && e.type == EventType.mouseDown && e.button == 0){
							if(item.itemtype == Item.ItemType.Consumable){
								UseConsumable (item, i, true);
							}
						}
					}
				} else {
					if(slotRect.Contains (e.mousePosition)){
						if (e.type == EventType.mouseUp && draggingItem) {
							inventory [i] = draggedItem;
							draggingItem = false;
							draggedItem = null;
						}
					}
				}

				if(tooltip ==""){
					showTooltip = false;
				}

				i++;
			}
		}
	}

	string CreateTooltip(Item item){
		tooltip = "<color=#4DA4BF>" + item.itemName + "</color>\n\n" + "<color=#f2f2f2>" + item.itemDesc + "</color>";
		return tooltip;
	}

	void RemoveItem(int id){
		for (int i = 0; i < inventory.Count; i++) {
			if(inventory[i].itemID == id){
				inventory [i] = new Item ();
				break;
			}
		}
	}
		
	void AddItem(int id){
		for (int i = 0; i < inventory.Count; i++) {
			if(inventory[i].itemName == null){
				for(int j = 0; j < database.items.Count; j++){
					if(database.items[j].itemID == id){
						inventory [i] = database.items [j];
					}
				}
				break;
			}
		}
	}

	bool InventoryContains(int id){
		bool result = false;
		for (int i = 0; i < inventory.Count; i++) {
			result = inventory [i].itemID == id;
			if(result){
				break;
			}
		}
		return result;
	}

	private void UseConsumable(Item item, int slot, bool deleteItem){
		switch (item.itemID) {
		case 1:
			{
				print ("used health potion");
				break;
			}
		case 2:
			{
				print ("used mana potion");
				break;
			}
		}
		if(deleteItem){
			inventory [slot] = new Item ();
		}
	}

	void SaveInventory(){
		for (int i = 0; i < inventory.Count; i++) {
			PlayerPrefs.SetInt ("Inventory " + i, inventory[i].itemID);
		}
	}

	void LoadInventory(){
		for(int i = 0; i < inventory.Count; i++)
			inventory [i] = PlayerPrefs.GetInt ("Inventory " + i, -1) >= 0 ? database.items[PlayerPrefs.GetInt("Inventory " + i)] : new Item() ;
	}
}
